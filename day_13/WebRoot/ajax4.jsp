<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'ajax2.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<script type="text/javascript">
		function createXMLHttpRequest() {
			try {
				return new XMLHttpRequest();
			} catch (e) {
				try {
					return new ActiveXobject("Msxml2.XMLHTTP");
				} catch (e) {
					try {
						return new ActiveXobject("Microsoft.XMLHTTP");
					} catch (e) {
						throw e;
					}
				}
			}
		}
		window.onload = function() {
			var username = document.getElementById("username");
			username.onblur = function() {
				var xmlHttp = createXMLHttpRequest();
				
				xmlHttp.open("POST","<c:url value='/BServlet'/>",true);
				xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
				
				xmlHttp.send("username" + username.value);
				
				xmlHttp.onreadystatechange = function() {
					if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
						var text = xmlHttp.responseText;
						var span = document.getElementById("span");
						if(text == 1) {
							span.innerHTML = "用户名已经被使用";
						} else {
							span.innerHTML = "";
						}
					}
				};
				
			};
		};
	</script>

  </head>
  
  <body>
    <button type="button" id= "btn">点击我</button>
    <h1 id="h1"></h1>
  </body>
</html>
