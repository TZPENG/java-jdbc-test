<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'ajax.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript">
		
		function createXMLHttpRequest() {
			try {
				return new XMLHttpRequest();
			} catch (e) {
				try {
					return new ActiveXobject("Msxml2.XMLHTTP");
				} catch (e) {
					try {
						return new ActiveXonject("Microsoft.XMLHTTP");
					} catch (e) {
						alert("你的浏览器好好哦！");
						throw e;
					}
				}
			}
		}
		window.onload = function() {//文档加载后执行
			var btn = document.getElementById("btn");
			btn.onclick = function() {
				//1.创建异步对象，XMLHttpRequset对象
				var xmlHttp = createXMLHttpRequest();
				
				//2.打开与服务器的连接
				xmlHttp.open("GET","<c:url value='/AServlet'/>",true);
				
				//3.发送请求
				xmlHttp.send(null);
				
				//4.给异步对象的事件监听
				xmlHttp.onreadystatechange = function () {
					if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
						var h1 = document.getElementById("h1");
						var text = xmlHttp.responseText;
						h1.innerHTML = text;
					}
				};
				
			};
		};
	</script>
	
  </head>
  <body>
    <button type="button" id= "btn">点击我</button>
    <h1 id="h1"></h1>
  </body>
</html>
