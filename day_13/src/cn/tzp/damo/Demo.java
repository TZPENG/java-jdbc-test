package cn.tzp.damo;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.junit.Test;

public class Demo {
	@Test
	public void show4() {
		Person p1 = new Person("zhangsan",12);
		Person p2 = new Person("lisi",22);
		List<Person> list = new ArrayList<Person>();
		list.add(p1);
		list.add(p2);
		
		System.out.println(JSONArray.fromObject(list).toString());
		
/*		JSONArray list = new JSONArray();
		list.add(p1);
		list.add(p2);
		System.out.println(list.toString());*/
	}
	
	@Test
	public void show3() {
		Person p1 = new Person("zhangsan",12);
		Person p2 = new Person("lisi",22);
		
		JSONArray list = new JSONArray();
		list.add(p1);
		list.add(p2);
		System.out.println(list.toString());
	}
	
	@Test
	public void show2() {
		JSONObject map = new JSONObject();
		map.put("name", "zhangsan");
		map.put("age", 12);
		map.put("gender", "male");
		System.out.println(map.toString());
	}
	
	@Test
	public void show() {
		Person person = new Person("zhangsan",12);
		JSONObject map = JSONObject.fromObject(person);
		System.out.println(map.toString());
	}
}
