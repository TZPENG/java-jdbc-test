package cn.tzp.custome.service;

import java.util.List;

import cn.tzp.custome.dao.CustomerDao;
import cn.tzp.custome.domain.Customer;

/*
 * 业务逻辑层
 */
public class CustomerService {
	private CustomerDao customerDao = new CustomerDao();
	
	public void add(Customer customer) {
		customerDao.add(customer);
	}
	
	public List<Customer> findAll() {
		return customerDao.findAll();
	}

	public Customer load(String cid) {
		return customerDao.load(cid);
	}

	public void edit(Customer c) {
		customerDao.edit(c);
	}

	public void delete(String cid) {
		customerDao.delete(cid);
	}

	public List<Customer> query(Customer c) {
		return customerDao.query(c);
	}
}
