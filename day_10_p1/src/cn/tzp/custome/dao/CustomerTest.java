package cn.tzp.custome.dao;

import org.junit.Test;

import cn.itcast.commons.CommonUtils;
import cn.tzp.custome.domain.Customer;

public class CustomerTest {
	private CustomerDao customerDao = new CustomerDao();
	@Test
	public void test() {
		Customer c = new Customer();
		for(int i=1; i<=300; i++) {
			c.setCid(CommonUtils.uuid());
			c.setCname("customer_" + i);
			c.setGender(i%2==0?"男":"女");
			c.setBirthday("2017-10-20");
			c.setCellphone("155431" + i);
			c.setEmail("customer_" + i + "@168.com");
			c.setDescription("我是客户" + "customer_" + i);
			customerDao.add(c);
		}
	}
}
