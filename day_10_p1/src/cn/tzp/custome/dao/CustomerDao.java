package cn.tzp.custome.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import cn.itcast.jdbc.TxQueryRunner;
import cn.tzp.custome.domain.Customer;

/*
 * 持久层
 */

public class CustomerDao {
	// 有错误
	private QueryRunner qr = new TxQueryRunner();

	// 添加功能
	public void add(Customer customer) {
		String sql = "insert into t_customer values(?,?,?,?,?,?,?)";
		Object[] params = { customer.getCid(), customer.getCname(),
				customer.getGender(), customer.getBirthday(),
				customer.getCellphone(), customer.getEmail(),
				customer.getDescription() };
		try {
			qr.update(sql, params);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// 全部查找
	public List<Customer> findAll() {
		try {
			String sql = "select * from t_customer";
			return qr.query(sql, new BeanListHandler<Customer>(Customer.class));
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}

	public Customer load(String cid) {
		try {
			String sql = "select * from t_customer where cid=?";
			return qr.query(sql, new BeanHandler<Customer>(Customer.class), cid);
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}

	public void edit(Customer c) {
		try {
			String sql = "update t_customer set cname=?,gender=?,birthday=?,cellphone=?,email=?,Description=? where cid=?";
			Object[] params = {c.getCname(),c.getGender(), c.getBirthday(),c.getCellphone(), c.getEmail(),c.getDescription(),c.getCid()};
			qr.update(sql, params);
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}

	public void delete(String cid) {
		try {
			String sql = "delete from t_customer where cid=?";
			qr.update(sql, cid);
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}

	public List<Customer> query(Customer c) {
		try {
			StringBuffer sql = new StringBuffer("select * from t_customer where 1=1");
			ArrayList<Object> params = new ArrayList<Object>();
			
			String cname = c.getCname();
			if(cname != null && !cname.trim().isEmpty()) {
				sql.append(" and cname like ?");
				params.add("%" + cname + "%");
			}
			
			String gender = c.getGender();
			if(gender != null && !gender.trim().isEmpty()) {
				sql.append(" and gender=?");
				params.add(gender);
			}
			
			String cellphone = c.getCellphone();
			if(cellphone != null && !cellphone.trim().isEmpty()) {
				sql.append(" and cellphone like ?");
				params.add("%" + cellphone + "%");
			}
			
			String email = c.getEmail();
			if(email != null && !email.trim().isEmpty()) {
				sql.append(" and email like ?");
				params.add("%" + email + "%");
			}
			
			String sql2 = sql.toString();
			Object[] params2 = params.toArray();
			
			return qr.query(sql2, new BeanListHandler<Customer>(Customer.class), params2);
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}

}















