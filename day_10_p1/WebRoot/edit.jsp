<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'edit.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <form action="<c:url value='/CustomerServlet'/>" method="post">
    	<input type="hidden" name="method" value="edit"/>
    	<input type="hidden" name="cid" value="${cstm.cid }">
    	<table  cellspacing="0" width="300px" style="position: relative; left: 50px">
    	<tr>
    		<td>姓名</td>
    		<td>
    			<input type="text" name="cname" value="${cstm.cname}"/>
			</td>
    	</tr>
    	<tr>
    		<td>性别</td>
    		<td>
				<input type="radio" name="gender" value="男" <c:if test="${cstm.gender eq '男'}">checked='checked'</c:if> />男
				<input type="radio" name="gender" value="女" <c:if test="${cstm.gender eq '女'}">checked='checked'</c:if>/>女				
			</td>
    	</tr>
    	<tr>
    		<td>生日</td>
    		<td>
    			<input type="text" name="birthday" value="${cstm.birthday}"/>
    		</td>
    	</tr>
    	<tr>
    		<td>手机号</td>
    		<td>
    			<input type="text" name="cellphone" value="${cstm.cellphone}"/>
    		</td>
    	</tr>
    	<tr>
    		<td>email</td>
    		<td>
    			<input type="text" name="email" value="${cstm.email}"/>
    		</td>
    	</tr>
    	<tr>
    		<td>描述</td>
    		<td>
    			<input type="text" name="description" value="${cstm.description}"/>
    		</td>
    	</tr>
    	<tr>
    		<td>
    			<input type="submit" value="编辑"/>
    		</td>
    	</tr>
    	
    </table>
    
    </form>
  </body>
</html>















