<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'list.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  	<table border="1px" cellspacing="0" width="1000px">
  			<tr style="font-weight: bolder;">
    			<td>用户ID</td>
    			<td>姓名</td>
    			<td>性别</td>
    			<td>生日</td>
    			<td>电话</td>
    			<td>email</td>
    			<td>描述</td>
    		</tr>
  		<c:forEach items="${requestScope.list }" var="clist">
    		<tr>
    			<td>${clist.cid }</td>
    			<td>${clist.cname }</td>
    			<td>${clist.gender }</td>
    			<td>${clist.birthday }</td>
    			<td>${clist.cellphone }</td>
    			<td>${clist.email }</td>
    			<td>${clist.description }</td>
    			<td>
    				<a href="<c:url value='/CustomerServlet?method=preEdit&cid=${clist.cid }'/>">编辑</a>
    				<a href="<c:url value='/CustomerServlet?method=delete&cid=${clist.cid }'/>">删除</a>
    			</td>
    		</tr>
    	</c:forEach>
  	</table>
  </body>
  </html>
