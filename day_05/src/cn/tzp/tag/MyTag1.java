package cn.tzp.tag;

import java.io.IOException;

import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.JspTag;
import javax.servlet.jsp.tagext.SimpleTag;

public class MyTag1 implements SimpleTag{
	private PageContext context;
	private JspFragment body;

	public void doTag() throws JspException, IOException {
		context.getOut().print("哈哈哈哈");
	}

	public JspTag getParent() {
		return null;
	}

	public void setJspBody(JspFragment body) {
		this.body = body;
	}

	public void setJspContext(JspContext context) {
		this.context = (PageContext)context;
	}

	public void setParent(JspTag arg0) {}

}
