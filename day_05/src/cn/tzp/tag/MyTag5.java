package cn.tzp.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class MyTag5 extends SimpleTagSupport {
	private boolean falg;

	public void setFalg(boolean falg) {
		this.falg = falg;
	}

	@Override
	public void doTag() throws JspException, IOException {
		if(falg) {
			this.getJspBody().invoke(null);
		} 
	}
}












