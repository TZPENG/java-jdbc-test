<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'index3.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  	<%
  	String[] strs = {"one","two"};
  	request.setAttribute("strs", strs);
  	%>
  	
  	<c:forEach items="${requestScope.strs }" var="str">
  		${str }
  	</c:forEach>
  	
<%--   	<hr/>
  	<c:forEach var="str" begin="1" end="10">
  		${str }<br/>
  	</c:forEach>
  	
  	<hr/> --%>
<%--     <c:if test="${empty param.name }">
    	你傻了吧？
    </c:if>
    <c:choose>
    	<c:when test="${empty param.name }">
    		nishaleba
    	</c:when>
    	<c:otherwise>怪我喽</c:otherwise>
    </c:choose> --%>
  </body>
</html>
