package cn.tzp.web.servlet;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import sun.misc.BASE64Encoder;

public class Download1Servlet extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String name = "D:/20.avi";
		//String filename = new String("小东家.avi".getBytes("gbk"),"iso-8859-1");
		String filename = filenameEcoding("小东家.avi",request);
		
		String contentType = this.getServletContext().getMimeType(name);
		String contentDisposition = "attachment;filename=" + filename;
		InputStream input = new FileInputStream(name);
		
		response.setHeader("Content-Type", contentType);
		response.setHeader("Content-Disposition", contentDisposition);
		
		ServletOutputStream output = response.getOutputStream();
		
		IOUtils.copy(input, output);
	}
	
	public static String filenameEcoding(String filename,HttpServletRequest request) throws IOException {
		String agent = request.getHeader("User-Agent");
		if(agent.contains("Firefox")) {
			BASE64Encoder base64Encoder = new BASE64Encoder();
			filename = "?utf-8?B?"
					+ base64Encoder.encode(filename.getBytes("utf-8"))
					+ "?=";
		} else if(agent.contains("MSIE")) {
			filename = URLEncoder.encode(filename,"utf-8");
		} else {
			filename = URLEncoder.encode(filename,"utf-8");
		}
		return filename;
	}
}













