package cn.tzp.web.servlet;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class Upload2Servlet extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//创建工厂
		DiskFileItemFactory factory = new DiskFileItemFactory();
		//创建解析器
		ServletFileUpload sfu = new ServletFileUpload(factory);
		
		try {
			//解析request
			List<FileItem> fileItemList = sfu.parseRequest(request);
			//获取FileItem
			FileItem fileItem1 = fileItemList.get(0);
			FileItem fileItem2 = fileItemList.get(1);
			
			System.out.println("普通表单项演示");
			System.out.println(fileItem1.getFieldName()+"="+fileItem1.getString("utf-8"));
			System.out.println("文本表单项演示");
			System.out.println("Content-Type:"+fileItem2.getContentType());
			System.out.println("文件名称：" + fileItem2.getName());
			System.out.println("size:" + fileItem2.getSize());
			
			//输出
			File destFile = new File("D:/我的IE.jpg");
			fileItem2.write(destFile);
			
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}

}
