package cn.tzp.web.servlet;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import cn.itcast.commons.CommonUtils;

public class Upload3Servlet extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//创建工厂
		DiskFileItemFactory factory = new DiskFileItemFactory();
		//创建解析器
		ServletFileUpload fileUpload = new ServletFileUpload(factory);
		//设置单个表项文件大小
	/*	fileUpload.setFileSizeMax(100 * 1024);*/
		
	/*	//设置整个表单文件大小
		fileUpload.setSizeMax(1024*1024);*/
		
		try {
			//解析文件
			List<FileItem> fileItemList = fileUpload.parseRequest(request);
			//获取文件
			FileItem fileItem = fileItemList.get(1);
			//获取文件保存路径
			String root = this.getServletContext().getRealPath("/WEB-INF/files");
			//获取文件名
			String filename = fileItem.getName();
			//处理重名
			int index = filename.lastIndexOf("\\");
			if(index!=-1) {
				filename = filename.substring(index+1);
			}
			//添加UUID
			String savename = CommonUtils.uuid() + "_" + filename;
			//处理保存文件目录
			int hasdcode = filename.hashCode();
			//转换为16进制
			String hex = Integer.toHexString(hasdcode);
			
			File dirFile = new File(root,hex.charAt(0) + "/" + hex.charAt(1));
			dirFile.mkdirs();
			
			File destFile = new File(dirFile,savename);
			fileItem.write(destFile);
			
		} catch(FileUploadException e) {
			System.out.println("11111111");
			if(e instanceof FileUploadBase.FileSizeLimitExceededException) {
				System.out.println("2222222");
				request.setAttribute("msg", "上传文件过大！");
				request.getRequestDispatcher("/form3.jsp").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
}
















