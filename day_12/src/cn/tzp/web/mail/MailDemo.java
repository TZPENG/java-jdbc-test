package cn.tzp.web.mail;

import java.io.File;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.junit.Test;

import cn.itcast.mail.AttachBean;
import cn.itcast.mail.Mail;
import cn.itcast.mail.MailUtils;

public class MailDemo {
	@Test
	public void test4() throws Exception {
		Session session = MailUtils.createSession("smtp.163.com", "tzp18243293319", "TwwwZZ111P5832Q5");
		
		Mail mail = new Mail("tzp18243293319@163.com", 
				"1922823993@qq.com", "这是最后一个", "哈哈");
		
		AttachBean attach = new AttachBean(new File("D:/0.jpg"),"好看么.jpg");
		mail.addAttach(attach);
		
		
		
		MailUtils.send(session, mail);
	}
	/*
	 * 带附件测试
	 */
	@Test
	public void test3() throws Exception {
		Properties props = new Properties();
		props.setProperty("mail.host", "smtp.163.com");
		props.setProperty("mail.smtp.auth", "true");
		
		Authenticator auth = new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("tzp18243293319","TwwwZZ111P5832Q5");
			}
		};
		
		Session session = Session.getInstance(props, auth);
		
		MimeMessage msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress("tzp18243293319@163.com"));
		msg.setRecipients(RecipientType.TO, "1922823993@qq.com");
		
		MimeMultipart multipart = new MimeMultipart();
		
		MimeBodyPart part1 = new MimeBodyPart();
		part1.setContent("我带附件了？","text/html;charset=utf-8");
		multipart.addBodyPart(part1);
		
		MimeBodyPart part2 = new MimeBodyPart();
		part2.attachFile(new File("D:/0.jpg"));
		part2.setFileName(MimeUtility.encodeText("啦啦啦.jpg"));
		multipart.addBodyPart(part2);
		
		msg.setContent(multipart);
		
		Transport.send(msg);
	}
	
	@Test
	public void test2() throws Exception{
		Properties props = new Properties();
		props.setProperty("mail.host", "smtp.163.com");
		props.setProperty("mail.smtp.auth", "true");
		
		Authenticator auth = new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("tzp18243293319","TwwwZZ111P5832Q5");
			}
		};
		
		Session session = Session.getInstance(props, auth);
		
		MimeMessage msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress("tzp18243293319@163.com"));
		msg.setRecipients(RecipientType.TO, "1922823993@qq.com");
		
		msg.setSubject("我是测试2");
		msg.setContent("你好么","text/html;charset=utf-8");
		
		Transport.send(msg);
	}
	
	@Test
	public void test() throws Exception {
		//创建session
		Properties props = new Properties();
		props.setProperty("mail.host", "smtp.163.com");
		props.setProperty("mail.smtp.auth", "true");
		
		Authenticator auth = new Authenticator(){
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("tzp18243293319","TwwwZZ111P5832Q5");
			}
		};
		
		Session session = Session.getInstance(props, auth);
		
		//创建mimessage
		MimeMessage msg = new MimeMessage(session);
		//设置发件人
		msg.setFrom(new InternetAddress("tzp18243293319@163.com"));
		//设置收件人
		msg.setRecipients(RecipientType.TO, "1922823993@qq.com");
		//设置抄送
		msg.setRecipients(RecipientType.CC, "2858414347@qq.com");
		//设置暗送
		msg.setRecipients(RecipientType.BCC, "1455921871@qq.com");
		
		msg.setSubject("大兄弟你好");
		msg.setContent("佟志鹏测试邮件","text/html;charset=utf-8");
		
		//发送
		Transport.send(msg);
	}
}






















