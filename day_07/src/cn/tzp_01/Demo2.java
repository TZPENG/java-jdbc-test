package cn.tzp_01;

import org.junit.Test;

public class Demo2 {
	@Test
	public void test()  {
		ThreadLocal<String> tl = new ThreadLocal<String>();
		//存
		tl.set("hello");
		//worl会覆盖hello
		tl.set("word");
		//区
		String s = tl.get();
		System.out.println(s);
		//删除
		tl.remove();
	}
}
