package cn.tzp_01;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Test;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class Demo {
	@Test
	public void test3() throws SQLException {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		Connection conn = dataSource.getConnection();
		System.out.println(conn);
	}
	
	@Test
	public void test2() throws Exception {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		dataSource.setDriverClass("com.mysql.jdbc.Driver");
		dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/mydb");
		dataSource.setUser("root");
		dataSource.setPassword("root");
		
		dataSource.setAcquireIncrement(5);
		dataSource.setInitialPoolSize(20);
		dataSource.setMaxPoolSize(50);
		dataSource.setMinPoolSize(2);
		
		Connection conn = dataSource.getConnection();
		System.out.println(conn);
	}
	
	@Test
	public void test() throws Exception {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/mydb");
		dataSource.setUsername("root");
		dataSource.setPassword("root");
		
		dataSource.setMaxActive(20);
		dataSource.setMinIdle(5);
		dataSource.setMaxWait(1000);
		
		Connection conn = dataSource.getConnection();
		System.out.println(conn);
		conn.close();
	}
}
























