package cn.tzp_01;

import java.sql.Connection;
import java.sql.SQLException;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class JdbcUtils {
	private static ComboPooledDataSource dataSource = new ComboPooledDataSource();
	
	public static Connection getConnection() throws SQLException {
		return dataSource.getConnection();
	}
	
	public static ComboPooledDataSource getComboPooledDataSource() {
		return dataSource;
	}
	
	public static void close(Connection conn) throws SQLException {
		conn.close();
	}
}
