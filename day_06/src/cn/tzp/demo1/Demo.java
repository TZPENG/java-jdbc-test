package cn.tzp.demo1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

public class Demo {
	private String driverClassName = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost:3306/mydb";
	private String user = "root";
	private String password = "root";
	
	@Test
	public void show5() {
		try {
			Connection conn = JdbcUtils.getConnection();
			System.out.println(conn);
		} catch (SQLException e) {
		}
	}
	
	@Test
	public void show4() {
		Connection conn = null;
		Statement st = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			// 注册驱动
			Class.forName(driverClassName);
			// 建立连接
			conn = DriverManager.getConnection(url, user, password);
			// 创建语句
/*			st = conn.createStatement();*/
			String sql = "insert into stu values(?,?)";
			ps = conn.prepareStatement(sql);
			ps.setString(1, "itcast_0004");
			ps.setString(2, "小冬瓜");
			// 执行语句
			ps.executeUpdate();
			// 处理数据
			
/*			int num = rs.getMetaData().getColumnCount();
			while(rs.next()) {
				for(int i=1; i<=num; i++) {
					System.out.print(rs.getObject(i));
					if(i!=num) {
						System.out.print(", ");
					}
				}
				System.out.println();
			}*/
/*			while (rs.next()) {
				String id = rs.getString("id");
				String name = rs.getString("name");
				System.out.println(id + "--" + name);
			}*/

		} catch (Exception e) {
			throw new RuntimeException();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e2) {
				throw new RuntimeException();
			} finally {
				try {
					if (st != null)
						st.close();
				} catch (Exception e2) {
					throw new RuntimeException();
				} finally {
					try {
						if (conn != null)
							conn.close();
					} catch (SQLException e) {
						throw new RuntimeException();
					}
				}
			}
		}
	}

	@Test
	public void show3() throws Exception {
		String driverClassName = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost:3306/mydb";
		String user = "root";
		String password = "root";
		// 注册驱动
		Class.forName(driverClassName);
		// 建立连接
		Connection conn = DriverManager.getConnection(url, user, password);
		// 创建语句
		Statement st = conn.createStatement();
		String sql = "select * from stu";
		// 执行语句
		ResultSet rs = st.executeQuery(sql);
		// 处理数据
		while (rs.next()) {
			String id = rs.getString("id");
			String name = rs.getString("name");
			System.out.println(id + "--" + name);
		}
		rs.close();
		st.close();
		conn.close();
	}

	@Test
	public void show2() throws Exception {
		String driverClassName = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost:3306/mydb";
		String user = "root";
		String password = "root";

		// 注册驱动
		Class.forName(driverClassName);
		// 建立连接
		Connection conn = DriverManager.getConnection(url, user, password);
		// 创建语句
		Statement stat = conn.createStatement();
		String sql = "insert into stu values('itcast_0002','王五')";
		// 执行语句
		int number = stat.executeUpdate(sql);
		System.out.println(number);
	}

	@Test
	public void show() throws Exception {
		String url = "jdbc:mysql://localhost:3306/mydb";
		String user = "root";
		String password = "root";
		// 注册驱动
		Class.forName("com.mysql.jdbc.Driver");
		// 建立连接
		Connection conn = DriverManager.getConnection(url, user, password);
		System.out.println(conn);

	}
}
