package cn.tzp_01;

import java.io.Serializable;

import javax.servlet.http.HttpSessionActivationListener;
import javax.servlet.http.HttpSessionEvent;

public class User implements HttpSessionActivationListener, Serializable {

	private static final long serialVersionUID = 1L;

	public void sessionDidActivate(HttpSessionEvent arg0) {
		System.out.println("我被序列化了");
	}

	public void sessionWillPassivate(HttpSessionEvent arg0) {
		System.out.println("我活了");
	}

}
