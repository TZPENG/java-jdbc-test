package cn.tzp.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class BFilter implements Filter {

	public void destroy() {
	}

	public void doFilter(ServletRequest requset, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		System.out.println("BFilterr#start...");
		chain.doFilter(requset, response);
		System.out.println("BFilterr#end...");
	}

	public void init(FilterConfig arg0) throws ServletException {
	}

}
