package cn.tzp.web.listener;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;

public class BListener implements ServletContextAttributeListener {

    public void attributeAdded(ServletContextAttributeEvent arg0) {
        System.out.println("添加了" + arg0.getName() + "=" + arg0.getValue());
    }

    public void attributeReplaced(ServletContextAttributeEvent arg0) {
        System.out.println("替换了" + arg0.getName() + "=" + arg0.getValue()+"," +arg0.getServletContext().getAttribute(arg0.getName()));
    }

    public void attributeRemoved(ServletContextAttributeEvent arg0) {
        System.out.println("移除了" + arg0.getName() + "=" + arg0.getValue());
    }
	
}
