package cn.tzp.commens;

import java.util.UUID;

public class CommonUtils {
	private CommonUtils() {}
	
	public static String getUuid() {
		return UUID.randomUUID().toString().replace("-", "").toUpperCase();
	}
}
