package cn.tzp.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BServlet extends HttpServlet {
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String s1 = request.getParameter("num1");
		String s2 = request.getParameter("num2");
		int num1 = Integer.parseInt(s1);
		int num2 = Integer.parseInt(s2);
		int result = num1 + num2;
		
		request.setAttribute("result", result);
		
		request.getRequestDispatcher("/jia/b.jsp").forward(request, response);
	}

}
