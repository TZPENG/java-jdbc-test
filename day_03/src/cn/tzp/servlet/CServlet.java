package cn.tzp.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CServlet extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String username = request.getParameter("username");
		
		if(username.equals("tzp")) {
			Cookie c = new Cookie("name", username);
			c.setMaxAge(60*60*24);
			response.addCookie(c);
			
			HttpSession session = request.getSession();
			session.setAttribute("username", "tzp");
			response.sendRedirect("/day_03/session2/succ1.jsp");
		} else {
			request.setAttribute("meg", "用户名或者密码错误");
			request.getRequestDispatcher("/session2/login.jsp").forward(request, response);
		}
	}

}
