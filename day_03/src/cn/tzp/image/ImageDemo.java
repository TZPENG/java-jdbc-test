package cn.tzp.image;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Test;

public class ImageDemo {
	@Test
	public void show2() throws Exception {
		VerifyCode vc = new VerifyCode();
		BufferedImage bi = vc.getImage();
		System.out.println(vc.getText());
		VerifyCode.output(bi, new FileOutputStream("D://xxx.jpg"));
	}
	
	@Test
	public void show() throws Exception {
		//得到图片缓冲区
		BufferedImage bi = new BufferedImage(70, 35, BufferedImage.TYPE_INT_RGB);
		//得到画笔
		Graphics2D g = (Graphics2D)bi.getGraphics();
		//调颜色
		g.setColor(Color.white);
		//画矩形
		g.fillRect(0, 0, 70, 35);
		//调画笔颜色
		g.setColor(Color.red);
		//写数字
		g.drawString("Hello", 20, 20);
		//输出
		ImageIO.write(bi, "JPEG", new FileOutputStream("D://xxx.jpg"));
	}
}
