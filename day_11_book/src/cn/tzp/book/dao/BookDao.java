package cn.tzp.book.dao;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import cn.itcast.jdbc.TxQueryRunner;
import cn.tzp.book.domain.Book;

public class BookDao {
	private QueryRunner qr = new TxQueryRunner();
	
	public List<Book> findAll() {
		try {
			String sql = "select * from book";
			return qr.query(sql, new BeanListHandler<Book>(Book.class));
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}
	
	public List<Book> findByCategory(int category) {
		try {
			String sql = "select * from book where category=?";
			return qr.query(sql, new BeanListHandler<Book>(Book.class), category);
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}
}
