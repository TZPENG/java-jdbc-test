package cn.tzp.book.web.filter;

import java.io.File;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StaticFilter implements Filter {
	private FilterConfig config;

	public void destroy() {}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest)req;
		HttpServletResponse response = (HttpServletResponse)res;
		
		String category = request.getParameter("category");
		String htmlPage = category + ".html";
		String htmlPath = config.getServletContext().getRealPath("/htmls");
		File file = new File(htmlPath, htmlPage);
		
		if(file.exists()) {
			response.sendRedirect(request.getContextPath()+"/htmls/"+htmlPage);
			return;
		}
		
		StaticResponse sr = new StaticResponse(response, file.getAbsolutePath());
		System.out.println(file.getAbsolutePath());
		
		chain.doFilter(request, sr);
		sr.sendRedirect(request.getContextPath()+"/htmls/"+htmlPage);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		this.config = fConfig;
	}

}
