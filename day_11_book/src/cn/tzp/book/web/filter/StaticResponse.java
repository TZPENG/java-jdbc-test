package cn.tzp.book.web.filter;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class StaticResponse extends HttpServletResponseWrapper {
	private PrintWriter pw;

	public StaticResponse(HttpServletResponse response,String path) {
		super(response);
		try {
			pw = new PrintWriter(path,"utf-8");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public PrintWriter getWriter() throws IOException {
		
		return pw;
	}

}
