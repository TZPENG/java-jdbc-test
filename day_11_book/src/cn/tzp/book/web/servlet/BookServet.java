package cn.tzp.book.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.servlet.BaseServlet;
import cn.tzp.book.dao.BookDao;

public class BookServet extends BaseServlet{
     private BookDao bookDao = new BookDao();
	
	public String findAll(HttpServletRequest request, HttpServletResponse arg1)
			throws ServletException, IOException {
		request.setAttribute("bookList", bookDao.findAll());
		return "f:/show.jsp";
	}
	
	public String findByCategory(HttpServletRequest request, HttpServletResponse arg1)
			throws ServletException, IOException {
		int category = Integer.parseInt(request.getParameter("category"));
		request.setAttribute("bookList", bookDao.findByCategory(category));
		return "f:/show.jsp";
	}
}
