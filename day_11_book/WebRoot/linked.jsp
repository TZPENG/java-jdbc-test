<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'linked.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <div align="center">
    	<h1>连接列表</h1>
    	<a href="<c:url value='/BookServet?method=findAll'/>">显示所有图书</a>
    	<a href="<c:url value='/BookServet?method=findByCategory&category=1'/>">显示JavaSE</a>
    	<a href="<c:url value='/BookServet?method=findByCategory&category=2'/>">显示JavaEE</a>
    	<a href="<c:url value='/BookServet?method=findByCategory&category=3'/>">显示FRAMEWORK</a>
    </div>
  </body>
</html>
