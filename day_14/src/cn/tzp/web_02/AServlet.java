package cn.tzp.web_02;

import java.io.IOException;

import javax.servlet.http.Part;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/BServlet")
@MultipartConfig
public class AServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		/**
		 * 以前不能有的方法都可以用了
		 */
		Part img = req.getPart("img");
		
		/**
		 * 获取part中数据
		 */
		
		//获取文件的mime类型
		System.out.println(img.getContentType());
		System.out.println(img.getSize());
		System.out.println(img.getName());
		System.out.println(img.getHeader("Content-Disposition"));
		img.write("D:/tonghzieee.jpg");
	}
}











