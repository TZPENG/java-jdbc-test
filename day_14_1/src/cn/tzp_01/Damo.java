package cn.tzp_01;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import org.junit.Test;

public class Damo {
	@Test
	public void test() {
		InvocationHandler h = new InvocationHandler() {
			@Override
			public Object invoke(Object proxy, Method method, Object[] args)
					throws Throwable {
				System.out.println("哈哈，啦啦");
				return null;
			}
		};
		Object o = Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[]{A.class,B.class}, h);
		
		A a = (A)o;
		B b = (B)o;
		a.show();
		b.show();
	}
}

interface A{
	public abstract void show();
}
interface B{
	public abstract void show();
}





















