package cn.tzp.web.filter;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class AFilter implements Filter {
	private FilterConfig config;

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		//获取servletContext
		ServletContext application = config.getServletContext();
		//获取map
		Map<String,Integer> map = (Map<String,Integer>)application.getAttribute("map");
		//获取用户ip
		String ip = request.getRemoteAddr();
		//添加到map中
		if(map.containsKey(ip)) {
			int num = map.get(ip);
			map.put(ip, num+1);
		} else {
			map.put(ip, 1);
		}
		
		
		
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		this.config = fConfig;
	}

}
