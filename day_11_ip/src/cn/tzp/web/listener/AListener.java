package cn.tzp.web.listener;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class AListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent arg0) {
    	//创建map
        Map<String,Integer> map = new LinkedHashMap<String,Integer>();
        //获取application
        ServletContext application = arg0.getServletContext();
        //将map添加到application域中
        application.setAttribute("map", map);
    }


    public void contextDestroyed(ServletContextEvent arg0) {
    }
	
}
