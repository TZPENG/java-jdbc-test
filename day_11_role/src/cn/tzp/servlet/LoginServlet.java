package cn.tzp.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginServlet extends HttpServlet {
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		
		String username = (String)request.getParameter("username");
		HttpSession session = request.getSession();
		
		if(username != null) {
			
			session.setAttribute("user", username);
		}
		
		if(username.equals("itcast")) {
			session.setAttribute("username", username);
		}
		
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}

}









