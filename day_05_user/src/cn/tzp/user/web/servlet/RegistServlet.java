package cn.tzp.user.web.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.commons.CommonUtils;
import cn.tzp.user.domain.User;
import cn.tzp.user.service.DaoException;
import cn.tzp.user.service.UserService;

public class RegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//设置编码
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=utf-8");
		
		//依赖UserService
		UserService userService = new UserService();
		
		//封装表单数据
		User form = CommonUtils.toBean(request.getParameterMap(), User.class);
		
		/*
		 * 校验表单数据
		 */
		//创建错误map
		Map<String,String> errors = new HashMap<String,String>();
		
		//验证用户名
		String username = form.getUsername();
		if(username == null || username.trim().isEmpty()) {
			//设置错误信息
			errors.put("username", "用户名不能为空！");
		} else if(username.length()<2 || username.length()>8) {
			//设置错误信息
			errors.put("username", "用户名必须在2~8位之间");
		}
		//验证密码
		String password = form.getPassword();
		if(password == null || password.trim().isEmpty()) {
			//设置错误信息
			errors.put("password", "密码不能为空！");
		} else if(password.length()<6 || password.length()>15) {
			//设置错误信息
			errors.put("password", "密码必须在6~15位");
		}
		//验证验证码
		String verifyCode = form.getVerifyCode();
		String session_verifyCode = (String)request.getSession().getAttribute("session_verifyCode");
		if(verifyCode ==null || verifyCode.trim().isEmpty()) {
			//设置错误信息
			errors.put("verifyCode", "验证码不能为空");
		} else if(verifyCode.length()!=4) {
			//设置错误信息
			errors.put("verifyCode", "验证码必须是4位");
		} else if(!session_verifyCode.equalsIgnoreCase(form.getVerifyCode()) ) {
			//设置错误信息
			errors.put("verifyCode", "验证码错误！");
		}
		
		//判断error是否为空
		if(errors != null && errors.size()>0) {
			//将错误信息保存到request
			request.setAttribute("errors", errors);
			//将form保存到request中
			request.setAttribute("user", form);
			//转发
			request.getRequestDispatcher("/user/regist.jsp").forward(request, response);
			//结束
			return;
		}
			
		/*
		 * 登陆功能
		 */
		try {
			userService.regist(form);
			response.getWriter().print("登陆成功<a href='"+ request.getContextPath() + "/user/login.jsp" +"'>跳转至登陆界面</a>");
		} catch (DaoException e) {
			//获取异常信息，保存到request中
			request.setAttribute("msg", e.getMessage());
			//保存表单信息			
			request.setAttribute("user", form);
			//转发到注册界面
			request.getRequestDispatcher("/user/regist.jsp").forward(request, response);
			
		}
	}

}
