package cn.tzp.user.web.servlet;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.vcode.utils.VerifyCode;

public class VerifyCodeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//创建验证码类
		VerifyCode verifyCode = new VerifyCode();
		//获取图片
		BufferedImage image = verifyCode.getImage();
		//将图片内容保存到session中
		request.getSession().setAttribute("session_verifyCode", verifyCode.getText());
		//相应给客户端
		VerifyCode.output(image, response.getOutputStream());
	}

}
