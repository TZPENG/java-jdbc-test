package cn.tzp.user.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.commons.CommonUtils;
import cn.tzp.user.domain.User;
import cn.tzp.user.service.UserService;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//处理编码
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=utf-8");
		
		//代码
		UserService userService = new UserService();
		
		//封装表单数据
		User form = CommonUtils.toBean(request.getParameterMap(), User.class);
		
		try {
			//调用login
			User user = userService.longin(form);
			//保存信息
			request.getSession().setAttribute("sessionUser", user);
			//跳转界面
			response.sendRedirect(request.getContextPath() + "/user/index.jsp");
		} catch (Exception e) {
			//保存错误信息
			request.setAttribute("msg", e.getMessage());
			//保存表单信息
			request.setAttribute("user", form);
			//打回
			request.getRequestDispatcher("/user/login.jsp").forward(request, response);
			
		}
		
		
		
		
	}

}


















