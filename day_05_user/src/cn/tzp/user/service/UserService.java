package cn.tzp.user.service;

/*
 * 业务逻辑层
 */

import cn.tzp.user.dao.DaoFactory;
import cn.tzp.user.dao.UserDao;
import cn.tzp.user.domain.User;

public class UserService {
	private UserDao userDao = DaoFactory.getUserDao();
	
	//注册功能
	public void regist(User user) throws DaoException {
		User _user = userDao.findByUsername(user.getUsername());
		if(_user != null) {
			throw new DaoException("用户已经存在!");
		}
		userDao.add(user);
	}

	public User longin(User form) throws DaoException {
		//获取数据库数据
		User user = userDao.findByUsername(form.getUsername());
		
		if(user == null) {
			throw new DaoException("用户名不存在");
		}
		
		if(!user.getPassword().equals(form.getPassword())) {
			throw new DaoException("密码错误");
		}
		
		return user;
	}
}


















