package cn.tzp.user.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import cn.tzp.user.dao.JdbcUtils;
import cn.tzp.user.dao.UserDao;
import cn.tzp.user.domain.User;

public class JdbcUserDaoImpl implements UserDao{
	private Connection conn = null;
	private PreparedStatement ps = null;
	private ResultSet rs = null;

	public void add(User user) {
		try {
			conn = JdbcUtils.getConnection();
			String sql = "insert into stu values(?,?)";
			ps = conn.prepareStatement(sql);
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			ps.executeUpdate();
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally{
			JdbcUtils.free(rs, ps, conn);
		}
	}

	public User findByUsername(String username) {
		try {
			conn = JdbcUtils.getConnection();
			String sql = "select * from stu where name=?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			rs = ps.executeQuery();
			
			User user = null;
			if(rs.next()) {
				user = new User();
				user.setUsername(rs.getString(1));
				user.setPassword(rs.getString(2));
			}
			return user;
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally{
			JdbcUtils.free(rs, ps, conn);
		}
		
	}

}





















