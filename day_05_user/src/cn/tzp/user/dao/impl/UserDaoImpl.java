
package cn.tzp.user.dao.impl;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import cn.tzp.user.dao.UserDao;
import cn.tzp.user.domain.User;

/*
 * 数据访问层
 */

public class UserDaoImpl implements UserDao{
	//依赖的数据库
	private String path = "D:/users.xml";
	
	//添加用户
	public void add(User user) {
		//创建解析器对象
		SAXReader reader = new SAXReader();
		try {
			//解析文件
			Document doucment = reader.read(path);
			//添加用户
			Element root = doucment.getRootElement();
			Element eleuser = root.addElement("user");
			eleuser.addAttribute("username", user.getUsername());
			eleuser.addAttribute("password", user.getPassword());
			
			//回写
			XMLWriter writer = new XMLWriter(new OutputStreamWriter(new FileOutputStream(path)
			, "utf-8"), OutputFormat.createPrettyPrint());
			writer.write(doucment);
			writer.close();
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}
	
	//按姓名进行查找
	public User findByUsername(String username) {
		//创建解析器
		SAXReader reader = new SAXReader();
		try {
			//解析xml文件
			Document document = reader.read(path);
			//通过XPATH得到元素对象
			Element element = (Element)document.selectSingleNode("//user[@username='"+ username +"']");
			//验证是否为空
			if(element==null) {
				return null;
			}
			//否则将元素信息封装到对象
			User user = new User();
			String attrUsername = element.attributeValue("username");
			String attrPassword = element.attributeValue("password");
			user.setUsername(attrUsername);
			user.setPassword(attrPassword);
			
			return user;
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}
	
}


















