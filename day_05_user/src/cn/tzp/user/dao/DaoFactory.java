package cn.tzp.user.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DaoFactory {
	private static Properties props = null;

	static {
		InputStream in = DaoFactory.class.getClassLoader().getResourceAsStream(
				"dao.properties");
		props = new Properties();
		try {
			props.load(in);
		} catch (Exception e) {
			throw new RuntimeException();
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private DaoFactory() {
	}

	public static UserDao getUserDao() {
		try {
			Class clazz = Class.forName(props.getProperty("cn.tzp.user.dao.UserDao"));
			return (UserDao)clazz.newInstance();
			
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}
}













