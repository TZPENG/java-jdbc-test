package cn.tzp.user.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class JdbcUtils {
	private static Properties props = null;
	
	static {
		InputStream in = JdbcUtils.class.getClassLoader().getResourceAsStream("dbconfig.properties");
		props = new Properties();
		try {
			props.load(in);
		} catch (IOException e) {
			throw new RuntimeException();
		}
		//注册驱动
		try {
			Class.forName(props.getProperty("driverClassName"));
		} catch (ClassNotFoundException e) {
			throw new RuntimeException();
		}
	}
	
	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(props.getProperty("url"), props.getProperty("user"), props.getProperty("password"));
	}
	
	public static void free(ResultSet rs,Statement st,Connection conn) {
		try {
			if(rs!=null)
				rs.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if(st!=null)
					st.close();
			} catch (Exception e) {
				throw new RuntimeException(e);
			} finally {
				try {
					if(conn!=null)
						conn.close();
				} catch (Exception e) {
					throw new RuntimeException(e);
				} 
			}
		}
	}
}

























