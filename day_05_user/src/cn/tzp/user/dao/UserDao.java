package cn.tzp.user.dao;

import cn.tzp.user.domain.User;

public interface UserDao {
	public void add(User user) ;
	
	public User findByUsername(String username);
}
