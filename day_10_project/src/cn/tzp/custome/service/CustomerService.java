package cn.tzp.custome.service;


import cn.tzp.custome.dao.CustomerDao;
import cn.tzp.custome.domain.Customer;
import cn.tzp.custome.domain.PageBean;

/*
 * 业务逻辑层
 */
public class CustomerService {
	private CustomerDao customerDao = new CustomerDao();
	
	public void add(Customer customer) {
		customerDao.add(customer);
	}
	
	public PageBean<Customer> findAll(int pageCode, int pageSize) {
		return customerDao.findAll(pageCode,pageSize);
	}

	public Customer load(String cid) {
		return customerDao.load(cid);
	}

	public void edit(Customer c) {
		customerDao.edit(c);
	}

	public void delete(String cid) {
		customerDao.delete(cid);
	}

	public PageBean<Customer> query(Customer c, int pageCode, int pageSize) {
		return customerDao.query(c,pageCode,pageSize);
	}
}













