package cn.tzp.custome.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import cn.itcast.jdbc.TxQueryRunner;
import cn.tzp.custome.domain.Customer;
import cn.tzp.custome.domain.PageBean;

/*
 * 持久层
 */

public class CustomerDao {
	private QueryRunner qr = new TxQueryRunner();

	// 添加功能
	public void add(Customer customer) {
		String sql = "insert into t_customer values(?,?,?,?,?,?,?)";
		Object[] params = { customer.getCid(), customer.getCname(),
				customer.getGender(), customer.getBirthday(),
				customer.getCellphone(), customer.getEmail(),
				customer.getDescription() };
		try {
			qr.update(sql, params);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// 全部查找
	public PageBean<Customer> findAll(int pageCode, int pageSize) {
		try {
			//创建对象
			PageBean<Customer> pageBean = new PageBean<Customer>();
			//设置pageCode
			pageBean.setPageCode(pageCode);
			//设置pageSize
			pageBean.setPageSize(pageSize);
			//设置totalRecord
			String sql = "select count(*) from t_customer";
			Number num = (Number)qr.query(sql, new ScalarHandler());
			int totalRecord = num.intValue();
			pageBean.setTotalRecord(totalRecord);
			
			//设置beanList
			sql = "select * from t_customer order by cname limit ?,?";
			List<Customer> list = qr.query(sql, new BeanListHandler<Customer>(Customer.class), (pageCode-1)*pageSize,pageSize);
			
			//返回
			pageBean.setBeanList(list);
			return pageBean;
			
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}

	public Customer load(String cid) {
		try {
			String sql = "select * from t_customer where cid=?";
			return qr.query(sql, new BeanHandler<Customer>(Customer.class), cid);
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}

	public void edit(Customer c) {
		try {
			String sql = "update t_customer set cname=?,gender=?,birthday=?,cellphone=?,email=?,Description=? where cid=?";
			Object[] params = {c.getCname(),c.getGender(), c.getBirthday(),c.getCellphone(), c.getEmail(),c.getDescription(),c.getCid()};
			qr.update(sql, params);
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}

	public void delete(String cid) {
		try {
			String sql = "delete from t_customer where cid=?";
			qr.update(sql, cid);
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}

/*	public List<Customer> query(Customer c, int pageCode, int pageSize) {
		try {
			StringBuffer sql = new StringBuffer("select * from t_customer where 1=1");
			ArrayList<Object> params = new ArrayList<Object>();
			
			String cname = c.getCname();
			if(cname != null && !cname.trim().isEmpty()) {
				sql.append(" and cname like ?");
				params.add("%" + cname + "%");
			}
			
			String gender = c.getGender();
			if(gender != null && !gender.trim().isEmpty()) {
				sql.append(" and gender=?");
				params.add(gender);
			}
			
			String cellphone = c.getCellphone();
			if(cellphone != null && !cellphone.trim().isEmpty()) {
				sql.append(" and cellphone like ?");
				params.add("%" + cellphone + "%");
			}
			
			String email = c.getEmail();
			if(email != null && !email.trim().isEmpty()) {
				sql.append(" and email like ?");
				params.add("%" + email + "%");
			}
			
			String sql2 = sql.toString();
			Object[] params2 = params.toArray();
			
			return qr.query(sql2, new BeanListHandler<Customer>(Customer.class), params2);
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}*/
	
	public PageBean<Customer> query(Customer c, int pageCode, int pageSize) {
		try {
			PageBean<Customer> pageBean = new PageBean<Customer>();
			pageBean.setPageCode(pageCode);
			pageBean.setPageSize(pageSize);
			
			StringBuffer preSql = new StringBuffer("select count(*) from t_customer");
			StringBuffer whereSql = new StringBuffer(" where 1=1");
			
			ArrayList<Object> params = new ArrayList<Object>();
			
			String cname = c.getCname();
			if(cname != null && !cname.trim().isEmpty()) {
				whereSql.append(" and cname like ?");
				params.add("%" + cname + "%");
			}
			
			String gender = c.getGender();
			if(gender != null && !gender.trim().isEmpty()) {
				whereSql.append(" and gender=?");
				params.add(gender);
			}
			
			String cellphone = c.getCellphone();
			if(cellphone != null && !cellphone.trim().isEmpty()) {
				whereSql.append(" and cellphone like ?");
				params.add("%" + cellphone + "%");
			}
			
			String email = c.getEmail();
			if(email != null && !email.trim().isEmpty()) {
				whereSql.append(" and email like ?");
				params.add("%" + email + "%");
			}
			
			Number num = (Number)qr.query(preSql.append(whereSql).toString(), new ScalarHandler(), params.toArray());
			pageBean.setTotalRecord(num.intValue());
			
			StringBuffer sql = new StringBuffer("select * from t_customer");
			StringBuffer limit = new StringBuffer(" limit ?,?");
			params.add((pageCode-1)*pageSize);
			params.add(pageSize);
			List<Customer> list = qr.query(sql.append(whereSql).append(limit).toString(), new BeanListHandler<Customer>(Customer.class), params.toArray());
			pageBean.setBeanList(list);
			
			return pageBean;
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}

}















