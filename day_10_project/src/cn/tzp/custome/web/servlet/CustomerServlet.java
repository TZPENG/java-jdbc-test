package cn.tzp.custome.web.servlet;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.itcast.commons.CommonUtils;
import cn.itcast.servlet.BaseServlet;
import cn.tzp.custome.domain.Customer;
import cn.tzp.custome.domain.PageBean;
import cn.tzp.custome.service.CustomerService;

/*
 * WEB层
 */

public class CustomerServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private CustomerService customerService = new CustomerService();
	
	public String add(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Customer customer = CommonUtils.toBean(request.getParameterMap(), Customer.class);
		customer.setCid(CommonUtils.uuid());
		customerService.add(customer);
		request.setAttribute("msg", "保存成功！");
		
		return "f:/msg.jsp";
	}
	
	public String findAll(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//获取当前页码
		int pageCode = getPageCode(request);
		//设置每页记录数
		int pageSize = 10;
		//获取PageBean
		PageBean<Customer> pageBean = customerService.findAll(pageCode,pageSize);
		
		pageBean.setUrl(getUrl(request));
		//保存到request域中
		request.setAttribute("pageBean", pageBean);
		//转发
		return "f:/list.jsp";
	}
	
	//获取当前页码
	private int getPageCode(HttpServletRequest request) {
		String pageCode = request.getParameter("pageCode");
		
		if(pageCode == null || pageCode.trim().isEmpty()) {
			return 1;
		}
		
		return Integer.parseInt(pageCode);
	}
	
	
/*	public String findAll(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		request.setAttribute("list", customerService.findAll());
		
		return "f:/list.jsp";
	}*/
	
	public String preEdit(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String cid = request.getParameter("cid");
		Customer customer = customerService.load(cid);
		request.setAttribute("cstm", customer);
		
		return "f:/edit.jsp";
	}
	public String edit(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Customer c = CommonUtils.toBean(request.getParameterMap(), Customer.class);
		customerService.edit(c);
		request.setAttribute("msg", "编辑成功！");
		
		return "f:/msg.jsp";
	}
	
	public String delete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String cid = request.getParameter("cid");
		customerService.delete(cid);
		request.setAttribute("msg", "删除成功！");
		
		return "f:/msg.jsp";
	}
	
/*	public String query(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Customer c = CommonUtils.toBean(request.getParameterMap(), Customer.class);
		List<Customer> list= customerService.query(c);
		request.setAttribute("list", list);
		
		return "f:/list.jsp";
	}*/
	
	public String query(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
/*		System.out.println(getUrl(request));*/
		
		Customer c = CommonUtils.toBean(request.getParameterMap(), Customer.class);
		c = encoding(c);
		//获取当前页码
		int pageCode = getPageCode(request);
		//设置每页记录数
		int pageSize = 10;
		
		PageBean<Customer> list= customerService.query(c,pageCode,pageSize);
		
		list.setUrl(getUrl(request));
		request.setAttribute("pageBean", list);
		
		return "f:/list.jsp";
	}
	
	private Customer encoding(Customer c) throws UnsupportedEncodingException {
		String name = c.getCname();
		String gender = c.getGender();
		String cellphone = c.getCellphone();
		String email = c.getEmail();
		
		if(name != null && !name.trim().isEmpty()) {
			c.setCname(new String(name.getBytes("ISO-8859-1"),"UTF-8"));
		}
		if(gender != null && !gender.trim().isEmpty()) {
			c.setGender(new String(gender.getBytes("ISO-8859-1"),"UTF-8"));
		}
		if(cellphone != null && !cellphone.trim().isEmpty()) {
			c.setCellphone(new String(cellphone.getBytes("ISO-8859-1"),"UTF-8"));
		}
		if(email != null && !email.trim().isEmpty()) {
			c.setEmail(new String(email.getBytes("ISO-8859-1"),"UTF-8"));
		}
		
		return c;
	}

	private String getUrl(HttpServletRequest request) {
		String s1 = request.getContextPath();
		String s2 = request.getServletPath();
		String s3 = request.getQueryString();
		
		if(s3.contains("&pageCode=")) {
			int index = s3.lastIndexOf("&pageCode=");
			s3 = s3.substring(0, index);
		}
		return s1 + s2 + "?" + s3;
	}
}
















