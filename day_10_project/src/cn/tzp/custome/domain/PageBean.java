package cn.tzp.custome.domain;

import java.util.List;

public class PageBean<T> {
	// 当前页码
	private int pageCode;
	/*
	 * // 总页数 private int totalPage;
	 */
	// 总记录数
	private int totalRecord;
	// 每页记录数
	private int pageSize;
	// 当前页记录
	private List<T> beanList;
	// url后的参数
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public PageBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PageBean(int pageCode, int totalPage, int totalRecord, int pageSize,
			List<T> beanList) {
		super();
		this.pageCode = pageCode;
		/* this.totalPage = totalPage; */
		this.totalRecord = totalRecord;
		this.pageSize = pageSize;
		this.beanList = beanList;
	}

	public int getPageCode() {
		return pageCode;
	}

	public void setPageCode(int pageCode) {
		this.pageCode = pageCode;
	}

	public int getTotalPage() {
		int total = totalRecord / pageSize;
		int totalPage = totalRecord % pageSize == 0 ? total : (total + 1);
		return totalPage;
	}

	/*
	 * public void setTotalPage(int totalPage) { this.totalPage = totalPage; }
	 */

	public int getTotalRecord() {
		return totalRecord;
	}

	public void setTotalRecord(int totalRecord) {
		this.totalRecord = totalRecord;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public List<T> getBeanList() {
		return beanList;
	}

	public void setBeanList(List<T> beanList) {
		this.beanList = beanList;
	}

	@Override
	public String toString() {
		return "PageBean [pageCode=" + pageCode + ", totalPage="
				+ this.getTotalPage() + ", totalRecord=" + totalRecord
				+ ", pageSize=" + pageSize + ", beanList=" + beanList + "]";
	}

}
