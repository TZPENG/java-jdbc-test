<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'list.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  	<center>
  		<table border="1px" cellspacing="0" width="1000px">
  			<tr style="font-weight: bolder;">
    			<td>用户ID</td>
    			<td>姓名</td>
    			<td>性别</td>
    			<td>生日</td>
    			<td>电话</td>
    			<td>email</td>
    			<td>描述</td>
    		</tr>
  		<c:forEach items="${pageBean.beanList }" var="clist">
    		<tr>
    			<td>${clist.cid }</td>
    			<td>${clist.cname }</td>
    			<td>${clist.gender }</td>
    			<td>${clist.birthday }</td>
    			<td>${clist.cellphone }</td>
    			<td>${clist.email }</td>
    			<td>${clist.description }</td>
    			<td>
    				<a href="<c:url value='/CustomerServlet?method=preEdit&cid=${clist.cid }'/>">编辑</a>
    				<a href="<c:url value='/CustomerServlet?method=delete&cid=${clist.cid }'/>">删除</a>
    			</td>
    		</tr>
    	</c:forEach>
  	</table>
  	</center>
  	<br/><br/>
  	<center>
  		第${pageBean.pageCode }页/共${pageBean.totalPage }页&nbsp;
  		
  		<a href="${pageBean.url}&pageCode=1">首页</a>
  		<c:if test="${pageBean.pageCode>1 }">
  		<a href="${pageBean.url}&pageCode=${pageBean.pageCode-1 }">上一页</a>
  		</c:if>
  		
  		
  		<c:choose>
  			
  			<c:when test="${pageBean.totalPage<=10 }">
  				<c:set var="begin" value="1"/>
  				<c:set var="end" value="${pageBean.totalPage }"/>
  			</c:when>
  			<c:otherwise>
  				
  				<c:set var="begin" value="${pageBean.pageCode-5}"/>
  				<c:set var="end" value="${pageBean.pageCode+4}"/>
  				
  				<c:if test="${begin<1}">
  					<c:set var="begin" value="1"/>
  					<c:set var="end" value="10"/>
  				</c:if>
  				
  				<c:if test="${end>pageBean.totalPage }">
  					<c:set var="begin" value="${pageBean.pageCode-9}"/>
  					<c:set var="end" value="${pageBean.totalPage }"/>
  				</c:if>
  			</c:otherwise>
  		</c:choose>
  		
  		
  		<c:forEach var="i" begin="${begin}" end="${end}">
  			<c:choose>
  				<c:when test="${i eq pageBean.pageCode}">
  					[${i}]
  				</c:when>
  				<c:otherwise>
  					<a href="${pageBean.url}&pageCode=${i}">${i}</a>
  				</c:otherwise>
  			</c:choose>
  			
  		</c:forEach>
  		
  		<c:if test="${pageBean.pageCode<pageBean.totalPage }">
  		<a href="${pageBean.url}&pageCode=${pageBean.pageCode+1 }">下一页</a>
  		</c:if>
  		<a href="${pageBean.url}&pageCode=${pageBean.totalPage }">尾页</a>
  	</center>
  </body>
  </html>
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
