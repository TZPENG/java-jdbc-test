<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'query.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  	<center>
  		 <h1>高级搜索</h1>
    <form action="<c:url value='/CustomerServlet'/>" method="get">
    	<input type="hidden" name="method" value="query"/>
    	<table width="300px">
    		<tr>
    			<td>用户姓名</td>
    			<td>
    				<input type="text" name="cname"/>
    			</td>
    		</tr>
    		<tr>
    			<td>用户性别</td>
    			<td>
    				<select name="gender">
    					<option value="">==请选择==</option>
    					<option value="男">男</option>
    					<option value="女">女</option>
    				</select>
    			</td>
    		</tr>
    		<tr>
    			<td>用户手机</td>
    			<td>
    				<input type="text" name="cellphone"/>
    			</td>
    		</tr>
    		<tr>
    			<td>用户邮箱</td>
    			<td>
    				<input type="text" name="email"/>
    			</td>
    		</tr>
    	</table>
    	<input type="submit" value="搜索"/>
    </form>
  	</center>
  </body>
</html>



















