package cn.tzp.service;

import cn.tzp.dao.UserDao;
import cn.tzp.domain.User;

public class UserService {
	private UserDao userDao = new UserDao();
	
	public User find() {
		return userDao.find();
	}
}
