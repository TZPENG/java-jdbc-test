package cn.tzp_01;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.junit.Test;

public class Demo {
	/*
	 * ScalarHandler的应用，单行单列
	 */
	@Test
	public void show6() throws Exception {
		QueryRunner qr = new QueryRunner(JdbcUtils.getComboPooledDataSource());
		String sql = "select count(*) from stu";
		
		Object list= qr.query(sql, new ScalarHandler<>());
		System.out.println(list);
	}
	
	/*
	 * MapListHandler的应用，多行
	 */
	@Test
	public void show5() throws Exception {
		QueryRunner qr = new QueryRunner(JdbcUtils.getComboPooledDataSource());
		String sql = "select * from stu";
		
		List<Map<String,Object>> list= qr.query(sql, new MapListHandler());
		System.out.println(list);
	}
	
	/*
	 * MapHandler的应用，单行
	 */
	@Test
	public void show4() throws Exception {
		QueryRunner qr = new QueryRunner(JdbcUtils.getComboPooledDataSource());
		String sql = "select * from stu where name=?";

		Object[] params = {"张三"};
		Map map = qr.query(sql, new MapHandler(), params);
		System.out.println(map);
	}
	
	/*
	 * BeanListHandler的应用，多行
	 */
	@Test
	public void show3() throws Exception {
		QueryRunner qr = new QueryRunner(JdbcUtils.getComboPooledDataSource());
		String sql = "select * from stu";
		List<Stu> list = qr.query(sql, new BeanListHandler<Stu>(Stu.class));
		System.out.println(list);
	}
	
	/*
	 * BeanHandler<T>的应用，单行单列
	 */
	@Test
	public void show2() throws SQLException {
		QueryRunner qr = new QueryRunner(JdbcUtils.getComboPooledDataSource());
		String sql = "select * from stu where name=?";
		Object[] params = {"张三"};
		
/*		ResultSetHandler<Stu> rsh = new ResultSetHandler<Stu>(){
			public Stu handle(ResultSet arg0) throws SQLException {
				// TODO Auto-generated method stub
				return null;
			}
		};*/
		
		//第一个现成类BeanHandler<T>
		Stu stu = qr.query(sql, new BeanHandler<Stu>(Stu.class), params);
		System.out.println(stu);
	}
	
	@Test
	public void show() throws SQLException {
		QueryRunner qr = new QueryRunner(JdbcUtils.getComboPooledDataSource());
		String sql = "insert into stu values(?,?)";
		Object[] params = {"李四","12345672"};
		qr.update(sql, params);
		
	}
}
