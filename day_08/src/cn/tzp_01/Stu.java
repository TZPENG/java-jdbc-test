package cn.tzp_01;

public class Stu {
	private String name = null;
	private String password = null;
	public Stu() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Stu(String name, String password) {
		super();
		this.name = name;
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "Stu [name=" + name + ", password=" + password + "]";
	}
	
	
}
