<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'login.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  <%
  String m = "";
  String s = (String)request.getAttribute("msg");
  if(s!=null) {
	  m = s;
  }
  %>
  	<h1>登录界面</h1>
  	<font color="red"><b><%=m %></b></font>
    <form action="/day_04/LoginServlet" method="post">
    	用户名：<input type="text" name="username"/><br/>
    	密   码：<input type="password" name="pwd"/><br/>
    	验证码：<input type="text" name="client" size="3"/>
    	<img id="img" src="/day_04/VerifyCodeServlet">
    	<a href="javascript:change()">换一张</a>
    	<br/>
    	<input type="submit" value="提交" />
    	<script type="text/javascript">
    		function change() {
    			var img = document.getElementById("img");
    			img.src = "/day_04/VerifyCodeServlet?image="+new Date().getTime();
    		}
    	</script>
    </form>
  </body>
</html>
