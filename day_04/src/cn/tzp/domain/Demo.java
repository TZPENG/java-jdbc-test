package cn.tzp.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.junit.Test;

import cn.itcast.commons.CommonUtils;

public class Demo {
	@Test
	public void show3() {
		Map<String,String> map = new HashMap<String,String>();
		map.put("username", "张三");
		map.put("password", "123456");
		
		User user = CommonUtils.toBean(map, User.class);
		System.out.println(user);
	}
	
	@Test
	public void show2() throws Exception {
		Map<String,String> map = new HashMap<String,String>();
		map.put("username", "张三");
		map.put("password", "123456");
		
		Set<String> set = map.keySet();
		for(String s:set) {
			String ss = map.get(s);
			System.out.println(ss);
		}
		
		User bean = new User();
		BeanUtils.populate(bean, map);
		System.out.println(bean);
	}
	
	@Test
	public void show() throws Exception {
		Class clazz = Class.forName("cn.tzp.domain.Person");
		Object bean = clazz.newInstance();
		
		BeanUtils.setProperty(bean, "name", "张山");
		BeanUtils.setProperty(bean, "age", "23");
		BeanUtils.setProperty(bean, "gender", "60");
		
		String ss = BeanUtils.getProperty(bean, "age");
		System.out.println(ss);
		System.out.println(bean);
	}
}
