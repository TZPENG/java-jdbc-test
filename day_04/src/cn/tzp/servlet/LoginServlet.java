package cn.tzp.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//处理中文问题
		request.setCharacterEncoding("utf-8");
		
		String code = (String)request.getSession().getAttribute("verifycode");
		String client = request.getParameter("client");
		if(!code.equalsIgnoreCase(client)) {
			request.setAttribute("msg", "验证码错误");
			request.getRequestDispatcher("/session/login.jsp").forward(request, response);
			return;
		}
		
		HttpSession session = request.getSession();
		String username = request.getParameter("username");
		
		if(username.equalsIgnoreCase("佟志鹏")) {
			session.setAttribute("username", username);
			response.sendRedirect("/day_04/session/succ.jsp");
		} else {
			request.setAttribute("msg", "您的用户名或密码错误");
			request.getRequestDispatcher("/session/login.jsp").forward(request, response);
		}
	}

}
