package cn.tzp.commonutils;

import java.util.Map;
import java.util.UUID;

import org.apache.commons.beanutils.BeanUtils;

public class CommonUtils {
	private CommonUtils() {}
	
	public static String getUuid() {
		return UUID.randomUUID().toString().replace("-", "").toUpperCase();
	}
	
	public static <T> T toBean(Map<Object,Object> map,Class<T> clazz) {
		try {
			T bean = clazz.newInstance();
			BeanUtils.populate(clazz.newInstance(), map);
			return bean;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
