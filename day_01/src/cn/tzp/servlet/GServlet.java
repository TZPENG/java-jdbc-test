package cn.tzp.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GServlet extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setHeader("cache-control", "no-cache");
		response.setHeader("pragma", "no-cache");
		response.setIntHeader("expires", -1);
		response.getWriter().print("hello");
		
	}

}
