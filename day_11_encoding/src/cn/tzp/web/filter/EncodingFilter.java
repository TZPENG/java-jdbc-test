package cn.tzp.web.filter;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class EncodingFilter extends HttpServletRequestWrapper {
	private HttpServletRequest request;

	public EncodingFilter(HttpServletRequest request) {
		super(request);
		this.request = request;
	}
	
	@Override
	public String getParameter(String name) {
		String param = request.getParameter(name);
		try {
			String paramname = new String(param.getBytes("ISO-8859-1"),"utf-8");
			return paramname;
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException();
		}
	}

}













