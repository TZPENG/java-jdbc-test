package cn.tzp.database;

import java.sql.Connection;
import java.sql.SQLException;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class JdbcUtils {
	private static ComboPooledDataSource dataSource = new ComboPooledDataSource();
	
	//事务专用连接
	private static ThreadLocal<Connection> tl = new ThreadLocal<Connection>();
	
	//获取连接
	public static Connection getConnection() throws SQLException {
		Connection con = tl.get();
		if(con != null) return con;
		return dataSource.getConnection();
	}
	
	//获取连接池
	public static ComboPooledDataSource getComboPooledDataSource() {
		return dataSource;
	}
	
	//开启事务
	public static void beginTransaction() throws SQLException {
		Connection con = tl.get();
		
		if(con != null) throw new SQLException("已经开启事务，不能重复开启");
		con = getConnection();
		con.setAutoCommit(false);
		
		tl.set(con);
	}
	
	//提交事务
	public static void commitTransaction() throws SQLException {
		Connection con = tl.get();
		
		if(con == null) throw new SQLException("未启事务，不能提交");
		con.commit();
		con.close();
		
		tl.remove();
	}
	
	//回滚事务
	public static void rollbackTransaction() throws SQLException {
		Connection con = tl.get();
		
		if(con == null) throw new SQLException("未启事务，不能回滚");
		con.rollback();

		tl.remove();
	}
	
	//关闭连接
	public static void releaseConnection(Connection connection) throws SQLException {
		Connection con = tl.get();
		
		if(con == null) connection.close();
		if(con != connection) connection.close();
	}
	
}


















