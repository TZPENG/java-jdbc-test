package cn.tzp.database;


import org.apache.commons.dbutils.QueryRunner;

public class AccountDao {
	public void update(String name,int money) throws Exception {
		QueryRunner qr = new TxQueryRunner();
		String sql = "update account set money = money + ? where name=?";
		Object[] param = {money, name};
		qr.update(sql, param);
	}
}
