package cn.tzp.database;

import org.junit.Test;


public class Demo {
	private AccountDao dao = new AccountDao();
	
	@Test
	public void test() throws Exception {
		try {
			JdbcUtils.beginTransaction();
			dao.update("张三", -100);
			int i = 10/0;
			dao.update("李四", 100);
			JdbcUtils.commitTransaction();
		} catch (Exception e) {
			JdbcUtils.rollbackTransaction();
			throw new Exception();
		}
	}
}
