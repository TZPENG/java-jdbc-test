package cn.tzp.servlet.forward;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class OneServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("OneServlet...");
		//设置响应头
		response.setHeader("aaa", "AAA");
		
		//与
		request.setAttribute("name", "zhangsan");
		
		//请求转发
		request.getRequestDispatcher("/TwoServlet").forward(request, response);
	}

}
