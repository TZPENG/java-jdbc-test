package cn.tzp.servlet;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out.println(request.getParameter("xx"));
		System.out.println(request.getParameter("yy"));
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String s = request.getParameter("username");
		System.out.println(s);
		String s2 = request.getParameter("pwd");
		System.out.println(s2);
		String[] str = request.getParameterValues("hobby");
		System.out.println(Arrays.toString(str));
		
		Map<String,String[]> m = request.getParameterMap();
		for(String name : m.keySet()) {
			String[] strr = m.get(name);
			System.out.println(name + "---" + Arrays.toString(strr));
		}
	}

}













