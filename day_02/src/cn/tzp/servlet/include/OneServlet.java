package cn.tzp.servlet.include;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class OneServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setHeader("haha", "aaa");
		//设置相应体
		response.getWriter().print("OneServlet");
		//请求包含
		request.getRequestDispatcher("/TwoServlet").include(request, response);
	}

}
