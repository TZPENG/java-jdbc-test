<%@ page language="java" import="cn.tzp.domain.*" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'a.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <%
    Address address = new Address();
    address.setCity("北京");
    address.setStreet("桂林路");
    
    Employee employee = new Employee();
    employee.setName("李四");
    employee.setSalary(2000);
    employee.setAddress(address);
    
    request.setAttribute("employee", employee);
    %>
    
    ${requestScope.employee.address.street }
    ${requestScope.employee.hehe }
  </body>
</html>
